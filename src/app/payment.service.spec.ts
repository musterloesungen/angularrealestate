import { TestBed } from '@angular/core/testing';
import { LoanCalculatorService } from './payment.service';

describe('LoanCalculatorService', () => {
  let service: LoanCalculatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoanCalculatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should calculate monthly payment correctly', () => {
    const principal = 100000; // Kapitalbetrag
    const annualInterestRate = 5; // Jährlicher Zinssatz in Prozent
    const loanTermYears = 30; // Laufzeit des Kredits in Jahren

    const monthlyPayment = service.calculateMonthlyPayment(principal, annualInterestRate, loanTermYears);
    expect(monthlyPayment).toBeCloseTo(536.82, 2);
  });

  it('should calculate total interest correctly', () => {
    const principal = 100000; // Kapitalbetrag
    const annualInterestRate = 5; // Jährlicher Zinssatz in Prozent
    const loanTermYears = 30; // Laufzeit des Kredits in Jahren

    const totalInterest = service.calculateTotalInterest(principal, annualInterestRate, loanTermYears);
    expect(totalInterest).toBeCloseTo(93655.58, 2);
  });

  it('should calculate total payment correctly', () => {
    const principal = 100000; // Kapitalbetrag
    const annualInterestRate = 5; // Jährlicher Zinssatz in Prozent
    const loanTermYears = 30; // Laufzeit des Kredits in Jahren

    const totalPayment = service.calculateTotalPayment(principal, annualInterestRate, loanTermYears);
    expect(totalPayment).toBeCloseTo(193655.58, 2);
  });
});
