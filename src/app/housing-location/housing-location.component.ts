import {Component, Input} from '@angular/core';
import {HousingLocation} from "../housinglocation";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-housing-location',
  standalone: true,
  imports: [
    RouterLink
  ],
  template: `
    <section class="listing">
      <img class="listing-photo" [src]="housingLocation.photo" alt="Exterior photo of {{housingLocation.name}}">
      <h2 class="listing-heading">{{ housingLocation.name }}</h2>
      <p class="listing-location">{{ housingLocation.city}}, {{housingLocation.state }}</p>
      <a [routerLink]="['/details', housingLocation.id]">Learn More</a>
    </section>
  `,
  styleUrl: './housing-location.component.css'
})
export class HousingLocationComponent {
  // Diese Daten kommen vom Parent Element per Data Binding
  // Und das ist ein Attribut in []
  @Input() housingLocation!: HousingLocation;
}
