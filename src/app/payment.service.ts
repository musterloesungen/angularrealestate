import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoanCalculatorService {

  constructor() { }

  // Berechnet die monatliche Rate eines Kredits
  calculateMonthlyPayment(principal: number, annualInterestRate: number, loanTermYears: number): number {
    const monthlyInterestRate = annualInterestRate / 12 / 100;
    const numberOfPayments = loanTermYears * 12;
    return (principal * monthlyInterestRate) / (1 - Math.pow((1 + monthlyInterestRate), -numberOfPayments));
  }

  // Berechnet den gesamten zu zahlenden Zins
  calculateTotalInterest(principal: number, annualInterestRate: number, loanTermYears: number): number {
    const totalPayment = this.calculateTotalPayment(principal, annualInterestRate, loanTermYears);
    return totalPayment - principal;
  }

  // Berechnet das Gesamtkapital
  calculateTotalPayment(principal: number, annualInterestRate: number, loanTermYears: number): number {
    const monthlyPayment = this.calculateMonthlyPayment(principal, annualInterestRate, loanTermYears);
    return monthlyPayment * loanTermYears * 12;
  }
}
