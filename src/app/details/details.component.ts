import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';

import {HousingLocation} from "../housinglocation";
import {HousingService} from "../housing.service";

@Component({
  selector: 'app-details',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: `./details.component.html`,
  styleUrl: './details.component.css'
})
export class DetailsComponent {
  housingLocation: HousingLocation | undefined = undefined

  applyForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    email: new FormControl('')
  });

  constructor(route: ActivatedRoute, private housingService: HousingService) {
    const id = Number(route.snapshot.params['id']);
    this.housingService.getHousingLocationById(id)
      .then((housingLocation) => this.housingLocation = housingLocation);
  }

  submitApplication() {
    this.housingService.submitApplication(
      this.applyForm.value.firstName ?? '',
      this.applyForm.value.lastName ?? '',
      this.applyForm.value.email ?? ''
    );
  }

}

