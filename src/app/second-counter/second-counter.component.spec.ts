import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondsCounterComponent } from './second-counter.component';

describe('SecondCounterComponent', () => {
  let component: SecondsCounterComponent;
  let fixture: ComponentFixture<SecondsCounterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SecondsCounterComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SecondsCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
