import {Component} from '@angular/core';
import {SignalService} from "../signal.service";

@Component({
  selector: 'app-second-counter',
  standalone: true,
  imports: [],
  template: '<p>Seconds since start: {{ secondsElapsed() }}</p>',
  styleUrl: './second-counter.component.css'
})
export class SecondsCounterComponent {
  secondsElapsed = this.signalService.seconds;

  constructor(private signalService: SignalService) { }
}
