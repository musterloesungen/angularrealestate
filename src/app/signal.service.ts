import { Injectable, Signal, signal } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SignalService {
  private secondsElapsed= signal<number>(0);

  constructor() {
    setInterval(() => {
      this.secondsElapsed.update(seconds => seconds + 1);
    }, 1000);
  }

  get seconds(): Signal<number> {
    return this.secondsElapsed;
  }
}
