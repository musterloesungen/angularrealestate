import {Component, inject, OnInit} from '@angular/core';
import {HousingLocationComponent} from "../housing-location/housing-location.component";
import { HousingLocation } from '../housinglocation';
import {NgForOf} from "@angular/common";
import {HousingService} from "../housing.service";
import {SecondsCounterComponent} from "../second-counter/second-counter.component";

@Component({
  selector: 'app-home', // <app-home>
  standalone: true,
  imports: [
    HousingLocationComponent,
    NgForOf,
    SecondsCounterComponent
  ],
  template: `
    <section>
      <form>
        <input  type="text" placeholder="Filter by city" #filter>
        <button (click)="filterResults(filter.value)" class="primary" type="button">Search</button>
      </form>
    </section>
    <app-second-counter></app-second-counter>
    <section class="results">
      <app-housing-location
        *ngFor="let housingLocation of filteredLocationList"
        [housingLocation]="housingLocation" ></app-housing-location>
    </section>
  `,
  styleUrl: 'home.component.css'
})
export class HomeComponent{
  housingLocationList: HousingLocation[] | undefined;
  filteredLocationList: HousingLocation[] | undefined= [];

  constructor(housingService: HousingService) {
    const housingListPromise = housingService.getAllHousingLocations()
    housingListPromise
      .then((housingLocationList) => {
      this.housingLocationList = housingLocationList;
      this.filteredLocationList = housingLocationList;
    })
      .catch((err) => console.error(err));
  }

  filterResults(text: string) {
    if (!text) {
      this.filteredLocationList = this.housingLocationList;
      return;
    }

    this.filteredLocationList = this.housingLocationList?.filter(
      housingLocation => housingLocation?.city.toLowerCase().includes(text.toLowerCase())
    );
  }}
